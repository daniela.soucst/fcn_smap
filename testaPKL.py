import cPickle as pickle
from datasets import get_image_pack_fn, ImageRecord
from data_provider import load_data, DataProvider
import numpy as np
import cv2

# with open('./image_pack.0.pkl') as f:
#  print('oia')
#  pack = pickle.load(f)
#  for record in pack:
#    map = record['map']
#    img_map = np.array(map, dtype='uint8')
#    cv2.imshow('img_map', img_map)
#    cv2.waitKey(0)
#    break

# //////////////////////////////////////////////
from utils import get_session
from fcn import FCN
ckpt
with get_session() as sess:
    fcn = FCN(sess=sess, name=name)
    if ckpt != "-1":
      fcn.load(ckpt)
    else:
      fcn.load_absolute(name)