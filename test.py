
import cPickle as pickle
from datasets import get_image_pack_fn, ImageRecord
from data_provider import load_data, DataProvider

# with open('data/gehler/image_pack.0.pkl') as f:
#  print('oia')
#  pack = pickle.load(f)
#  for record in pack:
#    map = record['map']
#
#  print()

# records = load_data(['g0'])
# print()

# python fc4.py test models/fc4/model_map/colorchecker_g1g2.ckpt -1 g0 fold0 model_map
# python fc4.py test models/fc4/model_map/colorchecker_g2g0.ckpt -1 g1 fold1 model_map
# python fc4.py test models/fc4/model_map/colorchecker_g0g1.ckpt -1 g2 fold2 model_map
# python combine.py /model_map/fold0_err.pkl /model_map/fold1_err.pkl /model_map/fold2_err.pkl


import random
import math
import numpy as np
from config import *
import cv2
from utils import rotate_and_crop
import tensorflow as tf
random.seed(0)
np.random.seed(0)


def load_image(fn):
    # file_path = self.get_img_directory() + '/images/' + fn
    file_path = '/home/daniela/PycharmProjects/fc4_py3/data/gehler/images/'+fn
    raw = np.array(cv2.imread(file_path, -1), dtype='float32')
    if fn.startswith('IMG'):
      # 5D3 images
      black_point = 129
    else:
      black_point = 1
    raw = np.maximum(raw - black_point, [0, 0, 0])
    return raw

def load_image_map(map_name):
    # file_path = self.get_map_directory() + 'MC/' + r.fn[:-4]+'.png'
    file_path ='/home/daniela/PycharmProjects/fcn_saliency/data/gehler/saliency/MC/'+map_name
    img_map = np.array(cv2.imread(file_path, 0),dtype='uint8')
    # img_map = np.array(cv2.imread(file_path, 0), dtype='float32')
#     print('img_map shap', img_map.shape)
    return img_map

# returns a function that takes array(int, 0,..resolution - 1)
def create_lut(f, resolution):
  num_samples = resolution

  lut = np.array(
      [f(x) for x in np.linspace(0, 1, num_samples)], dtype=np.float32)

  return lambda x: np.take(lut, x.astype('int32'))
def augment(ldr, illum, img_map):
    angle = (random.random() - 0.5) * AUGMENTATION_ANGLE
    scale = math.exp(random.random() * math.log(
        AUGMENTATION_SCALE[1] / AUGMENTATION_SCALE[0])) * AUGMENTATION_SCALE[0]
    s = int(round(min(ldr.shape[:2]) * scale))
    s = min(max(s, 10), min(ldr.shape[:2]))
    start_x = random.randrange(0, ldr.shape[0] - s + 1)
    start_y = random.randrange(0, ldr.shape[1] - s + 1)
    # Left-right flip?
    flip_lr = random.randint(0, 1)
    # Top-down flip?
    flip_td = random.randint(0, 1)
    color_aug = np.zeros(shape=(3, 3))
    for i in range(3):
        color_aug[i, i] = 1 + random.random(
        ) * AUGMENTATION_COLOR - 0.5 * AUGMENTATION_COLOR
        for j in range(3):
            if i != j:
                color_aug[i, j] = (random.random() - 0.5) * AUGMENTATION_COLOR_OFFDIAG

    def crop(img, illumination, img_map):
        if img is None:
            return None

        img_shape = img.shape[:2]
        #         print('image original shape:', img_shape)
        img = img[start_x:start_x + s, start_y:start_y + s]
        #         print('image original crop shape:', img.shape)
        img = rotate_and_crop(img, angle)
        img = cv2.resize(img, (FCN_INPUT_SIZE, FCN_INPUT_SIZE))

        img_map = cv2.resize(img_map, (img_shape[1], img_shape[0]))
        #         print('before map crop shape:', img_map.shape)
        img_map = img_map[start_x:start_x + s, start_y:start_y + s]
        #         print('after map crop shape:', img.shape)
        img_map = rotate_and_crop(img_map, angle)
        img_map = cv2.resize(img_map, (FCN_INPUT_SIZE, FCN_INPUT_SIZE))

        if AUGMENTATION_FLIP_LEFTRIGHT and flip_lr:
            img = img[:, ::-1]
            img_map = img_map[:, ::-1]
        if AUGMENTATION_FLIP_TOPDOWN and flip_td:
            img = img[::-1, :]
            img_map = img_map[::-1, :]

        img = img.astype(np.float32)
        img_map = img_map.astype(np.float32)
        new_illum = np.zeros_like(illumination)
        # RGB -> BGR
        illumination = illumination[::-1]
        for i in range(3):
            for j in range(3):
                new_illum[i] += illumination[j] * color_aug[i, j]
        if AUGMENTATION_COLOR_OFFDIAG > 0:
            # Matrix mul, slower
            new_image = np.zeros_like(img)
            for i in range(3):
                for j in range(3):
                    new_image[:, :, i] += img[:, :, j] * color_aug[i, j]
        else:
            img *= np.array(
                [[[color_aug[0][0], color_aug[1][1], color_aug[2][2]]]],
                dtype=np.float32)
            new_image = img
        new_image = np.clip(new_image, 0, 65535)
        img_map = np.clip(img_map, 0, 65535)
        def apply_nonlinearity(image):
            if AUGMENTATION_GAMMA != 0 or USE_CURVE: #AUGMENTATION_GAMMA = 0.0 e USE_CURVE=false
                res = 1024
                image = np.clip(image * (res * 1.0 / 65536), 0, res - 1)
                gamma = 1.0 + (random.random() - 0.5) * AUGMENTATION_GAMMA
                if USE_CURVE:
                    curve = get_random_curve()
                else:
                    curve = lambda x: x
                mapping = create_lut(lambda x: curve(x) ** gamma * 65535.0, res)
                return mapping(image)
            else:
                return image

        if SPATIALLY_VARIANT: #SPATIALLY_VARIANT = False
            split = new_image.shape[1] / 2
            new_image[:, :split] = apply_nonlinearity(new_image[:, :split])
            new_image[:, split:] = apply_nonlinearity(new_image[:, split:])
        else:
            new_image = apply_nonlinearity(new_image)

        new_illum = np.clip(new_illum, 0.01, 100)

        return new_image, new_illum[::-1], img_map

    return crop(ldr, illum, img_map)


#MAIN

illum = [0.327662, 0.341306, 0,331032]
raw = load_image('8D5U5524.png')
img_map = load_image_map('8D5U5524.png')
img = (np.clip(raw / raw.max(), 0, 1) * 65535.0).astype(np.uint16)
# cv2.imshow('img_map', img_map)


img, illum, img_map = augment(img, illum, img_map)
# cv2.imshow('img_raw', cv2.resize(img, (0, 0), fx=0.5, fy=0.5))
# cv2.imshow('img_map', img_map)
cv2.imshow('img',
           cv2.resize(
               np.power(img / 65535., 1.0 / 3.2), (0, 0),
               fx=0.5,
               fy=0.5))
cv2.imshow('crop_raw', img_map.astype(dtype=np.uint8))
cv2.imshow('crop_raw',
           cv2.resize(
               np.power(img_map / 65535., 1.0 / 3.2), (0, 0),
               fx=0.5,
               fy=0.5))
cv2.waitKey(0)
# config = tf.ConfigProto()
# config.gpu_options.per_process_gpu_memory_fraction = 0.80
# with tf.Session(config=config) as sess:
#     img_map_t = tf.convert_to_tensor(img_map)
#     img_t = tf.convert_to_tensor(img)
#
#
#     w, h = map(int, img_map_t.get_shape())
#     confidence = tf.reshape(img_map_t, shape=[w * h])
#     confidence = confidence / tf.norm(confidence)
#     # confidence = tf.nn.softmax(confidence)
#     confidence = tf.reshape(confidence, shape=[ w, h])
#     confidence = confidence.eval()
#
#     print()