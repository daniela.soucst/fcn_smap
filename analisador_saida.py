import cv2
import os

THRESHOLD = 0
FOLDER = 2
def main():

    file = open('folder'+str(FOLDER)+'.txt').readlines()
    try:
        os.mkdir('data/gehler/folder'+str(FOLDER)+'/maiorque'+str(THRESHOLD))
    except:
        pass
    try:
        os.mkdir('data/gehler/folder'+str(FOLDER)+'/menorouigual'+str(THRESHOLD))
    except:
        pass
    for line in file:
        words = line.split()
        img, err_fc4, err_mfc4, dif = words[0], float(words[1]), float(words[2]), float(words[3])
        img_file = cv2.imread('data/gehler/images_downsampled/' + img[:-4] + '.tif')
        if (dif > THRESHOLD):

            cv2.imwrite('data/gehler/folder'+str(FOLDER)+'/maiorque'+str(THRESHOLD)+"/"+img[:-4]+"_dif"+str(dif)+".png", img_file)

        else:
            cv2.imwrite('data/gehler/folder'+str(FOLDER)+'/menorouigual'+str(THRESHOLD)+"/"+img[:-4]+"_dif"+str(dif)+".png", img_file)



if __name__ == "__main__":
    main()