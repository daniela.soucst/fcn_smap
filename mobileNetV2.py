import tensorflow as tf
from config import *



def create_mobileNet(imgs):
    # IMG_SIZE = FCN_INPUT_SIZE # All images will be resized to 160x160

    IMG_SHAPE = (FCN_INPUT_SIZE, FCN_INPUT_SIZE, 3)
    # Create the base model from the pre-trained model MobileNet V2
    base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                                   include_top=False,
                                                   weights='imagenet')
    # base_model.summary()

    feature_batch = base_model(imgs)
    return feature_batch